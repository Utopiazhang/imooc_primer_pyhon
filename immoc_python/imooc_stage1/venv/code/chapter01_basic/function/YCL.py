#生活小助理YCL
import random

is_exit = False
phone_numbers_str = "匪警[110],火警[119],急救中心[120]"
weather_str = "北京,2019年1月12日,多云,8℃,-4℃,南风3级~上海,2019年1月12日,多云,8℃,-4℃,南风3级~广州,2019年1月12日,多云,8℃,-4℃,南风3级"

def generate_unionlotto(count):
    while count <= 0:
        count = int(input("输入有误，请重新输入（大于等于1的正整数）:"))
    for i in range(0,count):
        for j in range(1,7):
            red = random.randint(1,34)
            print(red, end = " ")
        blue = random.randint(1,17)
        print(blue)


def find_phone(keyword):
    phone_list = phone_numbers_str.split(",")
    for p in phone_list:
        if p.find(keyword) != -1:
            print(p)


def get_weather(city):
    city_list = weather_str.split("~")
    weather_data = {}
    for i in range(0, len(city_list)):
        w = city_list[i].split(",")
        weather = {"city": w[0], "date": w[1], "weather": w[2], "max": w[3], "min": w[4], "wind": w[5]}
        weather_data[weather["city"]] = weather
    if city in weather_data:
        return weather_data.get(city)
    else:
        return {}


while not is_exit:

    print("1-双色球随机选号")
    print("2-号码百事通")
    print("3-明日天气预报")
    print("0-结束程序")
    service_id = input("请输入功能编号：")

    if service_id == "1":
        n = input("您要生成几注双色球号码:")
        generate_unionlotto(int(n))
    elif service_id == "2":
        n = input("请输入您要查询的机构或电话号码：")
        find_phone(n)
    elif service_id == "3":
        n = input("请输入您要查询的城市：")
        w = get_weather(n)
        if "city" in w:
            print("{date} {city} {weather} {max}/{min} {wind}".format_map(w))
        else:
            print("未找到{0}的天气信息".format(n))

    elif service_id == "0":
        is_exit = True
    else:
        print("您输入的功能编号有误，请重申输入")

    print("==========================================================")
print("系统已退出，欢迎再次使用")




