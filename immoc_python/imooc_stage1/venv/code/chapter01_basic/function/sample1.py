def calc_excahnge_rate(amt, source, target = "USD"):
    return 0
# def health_check(name, age, height, weight,hr, hbp, lbp, glu):
#     return 0
# health_check(name= "张三", age=32, height=178, weight=80, hr= 70, lbp= 90, hbp= 120, glu= 4.3)
#使用*，*号后的参数必须使用关键字传参, *之前的参数，用关键字或位置都可以
# def health_check(name, age, *, height, weight,hr, hbp, lbp, glu):
#     return 0
# health_check(name="张三", age=32, height=178, weight=80, hr= 70, lbp= 90, hbp= 120, glu= 4.3)

def info(*, desc, birth, name='imooc'):
    # 使用format格式化字符串向控制台输出——imooc-程序员的梦工厂出生于2013年8月
    print("{name}-{desc}出生于{birth}".format(name = name, desc = desc, birth = birth))
# 调用函数，向函数内传入（"程序员的梦工厂"，"2013年8月"）
info(desc = "程序员的梦工厂", birth = "2013年8月")

def calc(a,b,c):
    return (a+b)*c
l = [1,5,10]
print(calc(*l))

def health_check(name, age, height, weight,hr, hbp, lbp, glu):
    return 0
def health_check(*,name, age, height, weight,hr, hbp, lbp, glu):
    return 0
#使用字典
argument_dict = {'name':"张三", 'age':32, 'height':178, 'weight':80, 'hr': 70, 'lbp': 90, 'hbp': 120, 'glu': 4.3}
health_check(**argument_dict)
#多个返回值
def get_detail_info():
    res_dict = {
        "employee": [
            {'name':"张三", 'age':32, 'height':178, 'weight':80, 'hr': 70}
            {'name': "李四", 'age': 32, 'height': 178, 'weight': 80, 'hr': 70}
        ],
        "asset":[{},{}],
        "project":[{},{}]
    }
    return res_dict
d = get_detail_info()
d.get("employee")[0].get("age")

def fun_dict(name,dept,hiredate,tel):
    # 使用format格式化字符串，使得向控制台输出结果——小葫芦隶属于技术部，电话:18795642135, 入职日期：2017-9-23,并向控制台输出结果
    print("{name}隶属于{dept}，电话：{tel}, 入职时间：{hiredate}".format(name=name,hiredate=hiredate,tel=tel,dept=dept))

# 创建字典dict1为{'name':'小葫芦','hiredate':'2017-9-23','tel':18795642135,'dept':'技术部'}
dict1 = {'name':'小葫芦','hiredate':'2017-9-23','tel':18795642135,'dept':'技术部'}
# 使用字典dict1作为参数传入函数fun_dict
fun_dict(**dict1)

def seq(num,num1,num2):
    # if判断num小于88
    if num < 88:
        #返回num1与num2的积
        return num1 * num2
    else:
        #返回num1与num2之和
        return num1 + num2
# 定义变量tuple1的值为(5,2,1)
tuple1 = (5,2,1)
# 调用函数，传入参数tuple1，并打印函数返回值
print(seq(*tuple1))