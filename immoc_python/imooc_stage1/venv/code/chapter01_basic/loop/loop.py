n = 1
# 使用while循环条件，控制输出的行数
while n <= 4:
    x = 1
    # 使用while循环条件，输出空格
    while x <= 3:
        # 条件成立时执行的语句

        if x + n >= 5:
            break
        print(" ", end="")
        x += 1
    y = 1
    # 使用while循环条件，输出*:
    while y <= 2 * n - 1:
        # 条件成立时执行的语句
        print("*", end="")
        y += 1
    # 换行输出
    print("")
    n = n + 1