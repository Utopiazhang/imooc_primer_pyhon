#查找1000以内的质数
#1.判断某个数字是否是质数
#2.连续判断多个数字是否是质数


def is_prime_number(number):
    if number % 2 == 0 and number != 2:
        return False
    else:
        i = 2
        while i <= number//2:
            if number % i == 0:
                return False
            i += 1
    return True

count  = 0
j = 2
while j <= 1000:
    if is_prime_number(j):
        print(j)
        count += 1
    j += 1

print("1000以内共有{}个质数".format(count))