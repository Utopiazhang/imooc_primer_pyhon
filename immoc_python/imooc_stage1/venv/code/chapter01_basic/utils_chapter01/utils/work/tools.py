import os.path
def get_file_type(file_path):
    """
    根据文件名称来判断文件类型
    :param file_name:文件名称
    :return:int 文件类型
    0: images
    1: word
    2: excel
    3: ppt
    -1: unknown
    """
    # unknown

    res = -1
    # check the file name
    if not os.path.isfile(file_path):
        return res

    path_name, ext = os.path.splitext(file_path)
    # lowcase all the extension
    ext.lower()

    if ext in ('.png', '.jpg', '.gif', '.bmp','.jpeg'):
        res = 0
    elif ext in ('.doc', 'docx'):
        res = 1
    elif ext in ('.xls', '.xlsx'):
        res = 2
    elif ext in ('.ppt', '.pptx'):
        res = 3
    return res