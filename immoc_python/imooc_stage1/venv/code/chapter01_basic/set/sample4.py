#集合的遍历
college1 = {"哲学","经济学","法学", "教育学"}
for c in college1:
    print(c)

#判断元素存在
print("哲学" in  college1)

#不支持按索引提取数据
#不支持更新操作

#新增数据（新增单个）
college1.add("计算机学")
#添加重复元素的语句会被忽略
college1.add("法学")
#新增多个(添加列表或元组)
college1.update(["生物学，工程学"])

#删除，remove如果删除不存在的元素是会报错
college1.remove("生物学")
#删除，discard，如果删除不存在的元素时，会忽略删除操作
college1.discard("生物")