#集合的数学运算
college1 = {"哲学","经济学","法学", "教育学"}
college2 = set(["金融学", "哲学","经济学","历史学","文学"])

#交集
#产生新的
c3 = college1.intersection(college2)
print(c3)
#更新旧的
college1.intersection_update(college2)
print(college1)

#并集
college1 = {"哲学","经济学","法学", "教育学"}
c4 = college1.union(college2)
print(c4)

#差集
#单向差集
c5 = college1.difference(college2)
print(c5)#->{'法学', '教育学'}
c6 = college2.difference(college1)
print(c6)#->{'金融学', '历史学', '文学'}
#双向差集
c7 = college1.symmetric_difference(college2)
print(c7)#->{'金融学', '法学', '教育学', '历史学', '文学'}
#更新原有集合，保留独有部分，单向差集
college1.difference_update(college2)
print(college1)#->{'法学', '教育学'}
#更新原有集合，保留各自独有部分，只更新一个的内容，双向差集
college1 = {"哲学","经济学","法学", "教育学"}
college1.symmetric_difference_update(college2)
print(college1)#-> {'历史学', '文学', '金融学', '教育学', '法学'}
