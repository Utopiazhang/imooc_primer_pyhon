# str1 = "Hello World!"
# str2 = 'Nice to meet you'
# str3 = str1, str2
#print(str3)
#print(type(str3))
#print(str1, 3, str2)
age = 20
#print("I am %d years old" % age)
# str4 = "tomorrow is sunny day"
# print(str4.find("is", 9, 11))
# str = "{}{}you".format("I","love")
# print(str)

# str =  "{2}.{1}.{0}".format("com","imooc","www")
# print(str)

name ="foo"
age = 25
height = 180.5
#传统方式：
str1 = "My name is"+name+"age:"+str(age)+"height: "+ str(height)
#格式化方式
str2 = "My name is {0}, height is {1}, class : {2}, age is {3}".format(name,height, "3-2",age)
#高级格式化用法（别名）
str3 = "My name is {p1}, height is {p2}, class : {p3}, age is {p4}".format(p1 = name, p2 = height,p3 =  "3-2",p4 = age)


index1 = "Nice to meet you".find("ee")#->9
index2 = "Nice to meet you， I need your help ".find("ee", 17)#->21
index3 = "Nice to meet you， I need your help ".find("ee", 17, 27)#->21

str1 =  "Nice to meet you， I need your help "
#返回True或False
res = "ee" in str1#->True

str = "aaabbbccc".replace("b", "d", 2) #->aaaddbccc
str = "aaabbbccc".replace("b", "d") #->aaaddbccc
print(str)
print(index1)
print(index2)
print(index3)
print(res)
str1 =  "Nice to meet you, I need your help "
l = str1.split(',')
#->['Nice to meet you', ' I need your help ']
print(l)