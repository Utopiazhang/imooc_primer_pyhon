str1 = format(1234.567, '0.2f') #小数保留2位 -> 1234.57（注意：返回值是str）
str2 = format(1234.567, ',') #千分位分隔符  -> 1,234.567
str3 = format(1234.567, '0,.2f') #千分位分隔符+小数点保留2位  -> 1,234.57

str = "请您向{}账户转账{:0,.3f}".format("88724", 12345.43123 )
#->请您向88724账户转账12,345.431
print(str1)
print(str2)
print(str3)
print(str)
print("叫%s, 今年%d岁，体重%.2f公斤"%("吴磊", 25,80.5))#->叫吴磊, 今年25岁，体重80.50公斤

#单双引号都可以
str2 = "my dog's name is Andy"
#单引号的使用场景
str1 = 'I told my friend "shi is beautiful"'
str = "MF" + str(8213)


