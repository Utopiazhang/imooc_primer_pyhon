l1 = ['a', 'b', 'c']
t1 = ('d', 'e', 'f')
s1 = 'abc123'
s2 = 'abc, 123'
r1 = range(1, 4)
#tuple -> list
print(list(t1))#'('d', 'e', 'f') -> ['d', 'e', 'f']
#str -> list 内容有结构的字符串可以分割成列表
print(s2.split(","))#''abc, 123' -> ['abc', ' 123']
#range -> list
print(list(r1))#'range(1, 4) -> [1, 2, 3]
#list-> tuple
print(tuple(l1))#['a', 'b', 'c'] -> ('a', 'b', 'c')
#str -> tuple
print(tuple(s1))#'abc123' -> ('a', 'b', 'c', '1', '2', '3')
#str -> tuple
print(tuple(s2))#'abc, 123' -> ('a', 'b', 'c', ',', ' ', '1', '2', '3')
#str -> tuplr
print(tuple(s2.split(",")))#'abc, 123' -> ('abc', ' 123')
#range -> tuple
print(tuple(r1))#range(1, 4) -> (1, 2, 3)

#str函数用于将单个数据转化围殴字符串
#join用于谅解字符串,但是要求序列中所有元素都i是字符串数据类型
#list -> str
print(str(l1))#"['a', 'b', 'c'] -> ['a', 'b', 'c']"
#list -> str  join前面的字符串用设置后面序列中每一个元素的分隔符
print("".join(l1))#['a', 'b', 'c'] ->"abc"
print(",".join(l1))#['a', 'b', 'c'] ->"a,b,c"

#tuple -> str
print(str(t1))#'('d', 'e', 'f') -> "('d', 'e', 'f')"
print(",".join(t1))#'('d', 'e', 'f') -> "d,e,f"
#将包含数字的序列的每一个元素转化成字符串、
s3 = ""
for i in r1:
    s3 += str(i)
print(s3)# range(1, 4) -> "1,2,3"
print(",".join(s3))#"123" -> "1,2,3"


