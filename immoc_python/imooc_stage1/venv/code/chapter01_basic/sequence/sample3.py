lst = ['春天','夏天','秋天','冬天']
lst2 =[]
for i in range(0, len(lst)):
    lst2.append("season"+str(i+1))
    lst2.append(lst[i])
dict = dict
print(lst2)
#tuple -> set
t = ("金融学","哲学","经济学","历史学","文学")
s1 = set(t)
print(s1)#->{'经济学', '金融学', '文学', '哲学', '历史学'}
#range -> set
print(set(range(0,10)))#->{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}

#set -> list
print(list(s1))#->['经济学', '金融学', '文学', '哲学', '历史学']
#set -> str
print(str(s1))#->"{'经济学', '金融学', '文学', '哲学', '历史学'}"
print(str(s1)[1])#->'   注意，第二位是个单引号
print(",".join(s1))#->经济学,金融学,文学,哲学,历史学

#set -> tuple
print(tuple(s1))#->('经济学', '金融学', '文学', '哲学', '历史学')