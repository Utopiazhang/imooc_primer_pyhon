#列表传统生成方法
lst1 = []
for i in range(10, 20):
    lst1.append(i * 10)
#列表时生成式
lst2 = [ i * 10 for i in range(10, 20)]
lst3 = [i * 10 for i in range(10, 20) if i% 2 == 0]
lst4 = [i * 10 for i in range(1, 5) for j in range(1, 5)]
lst5 = [i * j for i in range(1, 5) for j in range(1, 5)]

#字典生成式
lst6 = ['张三','李四','王五']
dict1 = {i+1:lst6[i] for i in range(0, len(lst6))}
print(dict1)#->{1: '张三', 2: '李四', 3: '王五'}

#集合时生成式
set1 = {i * j for i in range(1, 4) for j in range(1, 4) if i ==j}
print(set1)#->{1, 4, 9}

lst
