r1 = range(10, 20)
print(r1)#->range(10, 20)
print(r1[9])#->19
print(len(r1))#->10
print(r1[3:5])#->range(13, 15)

#增加步长
r2 = range(10, 20, 2)
print(r2)#->range(10, 20, 2)
print(r2[4])#->18
print(r2[0:2])#->range(10, 14, 2)
