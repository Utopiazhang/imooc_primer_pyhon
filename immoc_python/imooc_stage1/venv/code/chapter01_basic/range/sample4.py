#有四个数字1、2、3、4，能组成多少个互不相同且数字不重复的两位数
count = 0
# 定义一个空列表用于存放数据
result = []
for i in range(1, 5):
    # 使用for循环得到另一个数j
    for j in range(1, 5):
        if i != j:
            # 将数据添加到列表中
            result.append(i*10+j)
            count += 1
print(count)
# 输出得到的数据
print(result)