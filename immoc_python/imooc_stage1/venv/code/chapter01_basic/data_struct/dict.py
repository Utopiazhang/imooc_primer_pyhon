dict1 = {}
dict2 = {'name': '王峰', 'sex':"男",
         'hiredate' : '1997-10-20', 'grade':'A',
         'job': '销售', 'salary' : 1000, 'welfare' : 100}
dict3 = dict()
dict4 = dict(name= '王峰', sex = "男",
         hiredate ='1997-10-20', grade ='A',
         job= '销售', salary = 1000, welfare = 100)
dict5 = dict.fromkeys(("name", "sex", "score"),'N/A')


print(dict1)
print(dict2)
print(dict3)
print(dict4)
print(dict5)