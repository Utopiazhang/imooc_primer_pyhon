employee = {'name' : 'zhang', 'sex' : 'male', 'birthdate': '1989',
            'jab' : 'sale', 'salary': '10000',
            'welfare' : 100}

employee['name'] = 'Jone'
print(employee)
employee.update(salary = 12000, welfare = 200)
print(employee)

v1 = employee['name']
v2 = employee.get('name')
v3 = employee.get('name', 'default')
print(v1)
print(v2)
print(v3)
v = employee.pop('name')
print(type(v))
kv = employee.popitem()
print(type(kv))
res = employee.clear()
print(type(res))
