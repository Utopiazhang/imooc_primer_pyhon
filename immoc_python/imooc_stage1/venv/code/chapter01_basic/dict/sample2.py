emp1 = {'name': 'Jacky', 'grade': 'B'}
emp2 = {'name': 'Lily'}
# set default KV for dict, if the key exit, do nothing
emp2.setdefault('grade', 'C')


#key代表所有的键（特殊列表：dict_keys([key....])）
keys = emp1.keys()
#values代表所有的值(特殊列表：dict_values([value....])）
values = emp1.values()
#items代表所有的键值对:(特殊列表：dict_items([(key, value)...]))
items = emp1.items()

#返回值都是字典的视图对象，而不是普通的列表，视图对象会随着字典内容的变化而变化
emp1['hiredate'] = '1984-05-30'
# keys, values, items这三个变量指向的内存空间都会跟着变化，不需要重新获取复制
#因为视图就是原始数据的子集，原始数据变化，视图随之变化
#老版本字典格式化输出
emp_str_old = "姓名:%(name)s, 评级:%(grade)s, 入职时间:%(hiredate)s"%emp1
#新版本的字符串格式化
emp_str_new = "姓名:{name}, 评级:{grade}, 入职时间:{hiredate}".format_map(emp1)
print(emp_str_new)
