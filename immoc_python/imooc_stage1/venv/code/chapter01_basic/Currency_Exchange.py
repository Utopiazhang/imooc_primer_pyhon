service_menu = {}
service_menu['1'] = ("人民币转换美元", 0.15, '人民币', '美元' )
service_menu['2'] = ("美元转换人民币", 6.72, '美元', '人民币')
service_menu['3'] = ("人民币转换欧元", 0.13,"人民币", '欧元')
service_menu['0'] = ("结束程序",)

nunit_dict ={"人民币":'元','美元': '$','欧元': '欧元'}

is_exit = False
while not is_exit:
    print("**********欢迎使用货币转换系统**********")
    for k, v in service_menu.items():
        print("{}.{}".format(k, v[0]))
    service_id = input("请选择您需要的服务：")
    t_service = service_menu.get(service_id)
    if t_service is not None:
        if service_id != '0':
            currency = t_service[1]
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            print("欢迎使用{}服务".format(t_service[0]))
            input_money = input("请输入您需要转换的{}金额：".format(t_service[2]))
            print("您需要转换的{0}为:{1}{2} ".format(t_service[2], input_money, nunit_dict.get(t_service[2])))
            output_money = int(input_money) * currency
            print("您需要转换的{0}为:{1:0.2f}{2} ".format(t_service[3], output_money, nunit_dict.get(t_service[3])))
            print("============================================")
        else:
            is_exit = True
    else:
        print("您输入的选择有误,请重新输入")

print('感谢您的使用，祝您生活愉快，再见！')