t1 = (5,6,7) + (8,9,10)
#->(5, 6, 7, 8, 9, 10)
t2 = ('see', 'you')*2
#->('see', 'you', 'see', 'you')
print(t2)

t = ()
t = ('a', 'b', 1, 2, 3)

t[3]#正序索引
t[-1]#倒序索引
t[1:4]#范围取值，左闭右开

t3 = (['Zhang', 30, 5000],['Jon', 28, 2000])
item = t3[0]
print(item)
item[1] = 40
print(t3)
#->
#['Zhang', 30, 5000]
#(['Zhang', 40, 5000], ['Jon', 28, 2000])

t4 = ('see')*5#->seeseeseeseesee
t5 = (10)*5#->50
t6 = ('see',)*5#->('see', 'see', 'see', 'see', 'see')
t7 = (10,)*5#->(10, 10, 10, 10, 10)
print(t4)
print(t5)
print(t6)
print(t7)
t = (1,)*(2,3)
print(t)