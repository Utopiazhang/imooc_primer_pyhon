def pow_number(l):
    """
    calculate n^3, n in l
    :param l: list/tuple <int>
    :return: list of n^3
    """
    res = []
    for x in l:
        res.append(x*x*x)
    return res

def f(n):
    return n*n*n


def pow_num_use_map(l):
    return map(f, l)

def pow_num_use_map_lambda(l):
    return map(lambda n: n*n*n, l)

def use_map(data):
    # 使用result接收map实现各个元素的5次方功能
    result = map(lambda n: pow(n,5), data)
    return result

if __name__ == '__main__':
    l = [i for i in range(0,9)]
    res = pow_number(l)
    print(res)
    print('------------------')
    res = pow_num_use_map(l)
    print(list(res))
    print('------------------')
    res = pow_num_use_map_lambda(l)
    print(list(res))
    data = (2,4,6,8,10,12)
    # 调用use_map函数传入data，使用result接收
    result = use_map(data)
    print(tuple(result))