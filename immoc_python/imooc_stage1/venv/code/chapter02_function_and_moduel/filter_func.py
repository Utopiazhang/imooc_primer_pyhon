def is_odd(num):
    return num % 2 != 0


def use_filter_find_odd(l):
    """
    find odd in list/tuple
    :return: a list/tuple with odd
    """

    # filter(func, seq)
    res = filter(lambda n: n%2 != 0, l)
    res = filter(is_odd,l)
    return res

def use_filter_find_even(l):
    """
    find even in list/tuple
    :return: a list/tuple with even
    """

    # filter(func, seq)
    res = filter(lambda n: n%2 == 0, l)
    return res

def sorted_by_key(l):
    res = sorted(l, key = lambda x: x>0)
    return res

def sort_list(l):
    l.sort(key = lambda  x: x['age'])
    return l

if __name__ == '__main__':
    l = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    res = use_filter_find_odd(l)
    print(type(res))
    print(list(res))
    l = [3, 5, -4, -1, 0, -2, -6]
    res = sorted_by_key(l)
    print(res)
    l = [{"name": "xm", "age": 18},
         {"name": "xw", "age": 20},
         {"name": "xl", "age": 15}]
    res = sort_list(l)
    print(l[1]["name"])